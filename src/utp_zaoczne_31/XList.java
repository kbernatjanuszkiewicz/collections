package utp_zaoczne_31;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class XList<T> extends  ArrayList<T>{

    public XList(T ... values) {
        super(Arrays.asList(values));
    }

    public XList(Collection<T> values) {
        super(values);
    }

    public static <T> XList<T> of(T ... values) {
        return new XList<>(values);
    }

    public static <T> XList<T> of(Collection<T> values) {
        return new XList<>(values);
    }

    public static XList<String> charsOf(String s) {
        return s.chars().mapToObj(value -> (char) value).map(e -> Character.toString(e)).collect(Collectors.toCollection((Supplier<XList<String>>) XList::new));
    }

    public static XList<String> tokensOf(String s) {
        return tokensOf(s,"\\s");
    }

    public static XList<String> tokensOf(String s, String separator) {
        return Arrays.stream(s.split(separator)).collect(Collectors.toCollection((Supplier<XList<String>>) XList::new));
    }


    public XList<T> union(Collection<T> list) {
        List<T> collect = Stream.concat(stream(), list.stream()).collect(Collectors.toList());
        return new XList<>(collect);
    }

    public XList<T> union(T[] values) {
        return union(of(values));
    }

    public XList<T> diff(Collection<T> collection) {
        List<T> collect = stream().filter(e -> !collection.contains(e)).collect(Collectors.toList());
        return of(collect);
    }

    public XList<T> unique() {
        List<T> collect = stream().distinct().collect(Collectors.toList());
        return of(collect);
    }

    public XList<T> combine() {
        ArrayList<ArrayList<T>> combs = new ArrayList<ArrayList<T>>();

//        List<T> collect = stream().
        return null;
    }
}
