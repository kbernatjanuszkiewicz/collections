/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package utp_zaoczne_32;


import java.awt.image.AreaAveragingScaleFilter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class CustomersPurchaseSortFind<T> {
    List<String> lines;
    List<String> split_list;
    public CustomersPurchaseSortFind() {}



    public List<String> readFile(String fileName) {
        lines = Collections.emptyList();
        try {
            lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public List<String> showSortedBy(String selector) {
        String fname = System.getProperty("user.home") + "\\customers.txt";
        List<String> inputList = readFile(fname);


        if(selector.equals("Nazwiska")) {
            Collections.sort(inputList, Comparator.comparing(s -> s.substring(7)));
            System.out.println(selector);
            inputList.forEach(s -> System.out.println(s));
        }

        if(selector.equals("Koszty")) {
            List<String> properties = readFile(fname);
            return properties.stream()
                    .flatMap(s -> Arrays.stream(s.split(";")))
//                    .filter(s -> s.matches("[a-z]"))
//                    /peek(s -> System.out.println(s))
//                    .limit(5)

                    //.filter(s -> s.matches("[0-9]+"))
                    .collect(toList());






//            List<String> strings = Pattern.compile(";")
//                    .splitAsStream(listString)
//                    .collect(Collectors.toList());
//            System.out.println(strings);
//            System.out.println(strings.get(3));
//            double sum = Double.parseDouble(strings.get(3));
//            System.out.println(sum);


//           String[] dupa = listString.split(";");
//           for(String s : dupa) {
//               System.out.print(s);
//           }

//            Map<String, String> map = (Map<String, String>) Arrays.stream(listString.split(";"))
//                    .collect(Collectors.toList());

//            map.forEach(s  -> System.out.println(s));

        }




        return inputList;
    }





}
