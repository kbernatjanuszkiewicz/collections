/**
 *
 *  @author Bernat-Januszkiewicz Kacper S16508
 *
 */

package utp_zaoczne_32;


public class Main {

  public static void main(String[] args)  {
    CustomersPurchaseSortFind cpsf = new CustomersPurchaseSortFind();
    String fname = System.getProperty("user.home") + "\\customers.txt";
    cpsf.readFile(fname);
    System.out.println();
    cpsf.showSortedBy("Nazwiska");
    System.out.println();
    System.out.println(cpsf.showSortedBy("Koszty"));
//
//    String[] custSearch = { "c00001", "c00002" };
//
//    for (String id : custSearch) {
//      cpsf.showPurchaseFor(id);
//    }

    System.out.println();
  }

}
